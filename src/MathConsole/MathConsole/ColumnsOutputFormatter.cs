﻿namespace MathConsole
{
    public class ColumnsOutputFormatter : IOutputFormatter
    {
        public void FormatAndWriteOutput(TextWriter output, int[] values)
        {
            foreach (var value in values)
            {
                output.WriteLine(value);
            }
        }
    }
}