﻿namespace MathConsole
{
    public static class DefaultConstants
    {
        public const string OutputType = "console";
        public const string OutputFilePath = "text.txt";
        public const string OutputFormat = "line";
        public const int MatrixColumns = 3;
        public const string BackgroundColorName = "Black";
        public const string ForegroundColorName = "White";
        public const string LineOutputFormat = "line";
        public const string ColumnsOutputFormat = "columns";
        public const string MatrixOutputFormat = "matrix";
    }
}