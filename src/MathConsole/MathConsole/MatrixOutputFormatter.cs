﻿namespace MathConsole
{
    public class MatrixOutputFormatter : IOutputFormatter
    {
        private readonly int matrixColumns;

        public MatrixOutputFormatter(int matrixColumns)
        {
            this.matrixColumns = matrixColumns;
        }

        public void FormatAndWriteOutput(TextWriter output, int[] values)
        {
            for (int i = 0; i < values.Length; i += matrixColumns)
            {
                for (int j = 0; j < matrixColumns && i + j < values.Length; ++j)
                {
                    output.Write($"{values[i + j]}\t");
                }
                output.WriteLine();
            }
        }
    }
}