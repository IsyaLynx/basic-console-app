﻿namespace MathConsole
{
    public class LineOutputFormatter : IOutputFormatter
    {
        public void FormatAndWriteOutput(TextWriter output, int[] values)
        {
            foreach (var value in values)
            {
                output.Write($"{value} ");
            }
            output.WriteLine();
        }
    }
}