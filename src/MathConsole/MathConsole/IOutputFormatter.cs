﻿namespace MathConsole
{
    public interface IOutputFormatter
    {
        void FormatAndWriteOutput(TextWriter output, int[] values);
    }
}