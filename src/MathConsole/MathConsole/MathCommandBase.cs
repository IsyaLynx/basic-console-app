﻿using McMaster.Extensions.CommandLineUtils;

namespace MathConsole
{
    abstract class MathCommandBase
    {
        [Option("-n|--number", "The number parameter", CommandOptionType.SingleValue)]
        public int Number { get; }

        [Option("-o|--output <TYPE>", "Output type. Allowed values: console, file.", CommandOptionType.SingleValue)]
        private string OutputType { get; } = DefaultConstants.OutputType;

        [Option("-f|--file <path>", "Output file path", CommandOptionType.SingleValue)]
        public string OutputFilePath { get; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, DefaultConstants.OutputFilePath);

        [Option("-fmt|--format <FORMAT>", "Output format. Allowed values: line, columns, matrix.", CommandOptionType.SingleValue)]
        public string OutputFormat { get; } = DefaultConstants.OutputFormat;

        [Option("-c|--columns <COLUMNS>", "Number of columns for matrix format.", CommandOptionType.SingleValue)]
        public int MatrixColumns { get; } = DefaultConstants.MatrixColumns;

        [Option("-bg|--background-color <COLOR>", "Background color. Allowed values: Black, Blue, Green, Cyan, Red, Magenta, Yellow, White.", CommandOptionType.SingleValue)]
        public string BackgroundColorName { get; } = DefaultConstants.BackgroundColorName;

        [Option("-fg|--foreground-color <COLOR>", "Foreground color. Allowed values: Black, Blue, Green, Cyan, Red, Magenta, Yellow, White.", CommandOptionType.SingleValue)]
        public string ForegroundColorName { get; } = DefaultConstants.ForegroundColorName;

        public void OnExecute(CommandLineApplication app)
        {
            if (OutputType == OutputFilePath && string.IsNullOrWhiteSpace(OutputFilePath))
            {
                Console.WriteLine("Error: Output file path is not specified.");
                app.ShowHelp();
                return;
            }

            using (TextWriter outputWriter = CreateOutputWriter(OutputType, OutputFilePath))
            {
                Execute(outputWriter, OutputFormat, MatrixColumns);
            }
        }

        protected virtual TextWriter CreateOutputWriter(string outputType, string filePath)
        {
            if (outputType == OutputFilePath)
            {
                return new StreamWriter(filePath);
            }
            else
            {
                return new StreamWriter(Console.OpenStandardOutput()) { AutoFlush = true };
            }
        }

        protected virtual void Execute(TextWriter output, string outputFormat, int matrixColumns)
        {
            throw new NotImplementedException();
        }

        protected void FormatAndWriteOutput(TextWriter output, string outputFormat, int matrixColumns, int[] values)
        {
            SetConsoleColors();

            IOutputFormatter formatter = CreateFormatter(outputFormat, matrixColumns);

            formatter.FormatAndWriteOutput(output, values);

            ResetConsoleColors();
        }

        private IOutputFormatter CreateFormatter(string outputFormat, int matrixColumns)
        {
            switch (outputFormat)
            {
                case DefaultConstants.LineOutputFormat:
                    return new LineOutputFormatter();
                case DefaultConstants.ColumnsOutputFormat:
                    return new ColumnsOutputFormatter();
                case DefaultConstants.MatrixOutputFormat:
                    return new MatrixOutputFormatter(matrixColumns);
                default:
                    return new LineOutputFormatter();
            }
        }

        private void SetConsoleColors()
        {
            if (BackgroundColorName != DefaultConstants.BackgroundColorName || ForegroundColorName != DefaultConstants.ForegroundColorName)
            {
                ConsoleColor bgColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), BackgroundColorName, true);
                ConsoleColor fgColor = (ConsoleColor)Enum.Parse(typeof(ConsoleColor), ForegroundColorName, true);

                Console.BackgroundColor = bgColor;
                Console.ForegroundColor = fgColor;
            }
        }

        private void ResetConsoleColors()
        {
            if (BackgroundColorName != DefaultConstants.BackgroundColorName || ForegroundColorName != DefaultConstants.ForegroundColorName)
            {
                Console.ResetColor();
            }
        }
    }
}