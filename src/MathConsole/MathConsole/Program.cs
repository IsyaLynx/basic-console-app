﻿using McMaster.Extensions.CommandLineUtils;
using System;
using System.IO;

namespace MathConsole
{
    partial class Program
    {
        public static int Main(string[] args)
        {
            var app = new CommandLineApplication();

            app.HelpOption();

            app.Command<CalcFibCommand>("Fib", c => c.Conventions.UseDefaultConventions());
            app.Command<CalcFerCommand>("Fer", c => c.Conventions.UseDefaultConventions());

            return app.Execute(args);
        }
    }
}