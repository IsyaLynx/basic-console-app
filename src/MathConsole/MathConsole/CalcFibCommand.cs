﻿using McMaster.Extensions.CommandLineUtils;

namespace MathConsole
{ 
    class CalcFibCommand : MathCommandBase
    {
        protected override void Execute(TextWriter output, string outputFormat, int matrixColumns)
        {
            int n1 = 0, n2 = 1, n3, i;
            int[] values = new int[Number];
            values[0] = n1;
            values[1] = n2;
            for (i = 2; i < Number; ++i)
            {
                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
                values[i] = n3;
            }
            FormatAndWriteOutput(output, outputFormat, matrixColumns, values);
        }
    }
}