﻿using McMaster.Extensions.CommandLineUtils;

namespace MathConsole
{
    class CalcFerCommand : MathCommandBase
    {
        protected override void Execute(TextWriter output, string outputFormat, int matrixColumns)
        {
            int[] values = new int[Number];
            for (int i = 0; i < Number; ++i)
            {
                values[i] = (int)((Math.Pow(2, Math.Pow(2, i)) + 1));
            }
            FormatAndWriteOutput(output, outputFormat, matrixColumns, values);
        }
    }
}